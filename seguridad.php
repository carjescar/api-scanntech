<?php
class ApiScanntech{

    //DATOS DE USUARIO
    // public $URL_BASE = "http://test.parceiro.scanntech.com";
    // public $user = "integrador_test@layuntamayorista.com.ar";
    // public $passwd = "integrador";
    // public $operacion = "POST";
 
    //FUNCION PARA ARMAR CABECERA DE AUTORIZACION ( USUARIO : PASS)
    static public function convertir_base64($user, $passwd) {

        $strAutenticacion = $user . ":" . $passwd;
		$datos = null;
        $datos = utf8_encode($strAutenticacion);
        $strAutenticacion_Base64 = base64_encode($datos);
        return "Basic " + $strAutenticacion_Base64;
    }//Fin funcion convertir_base64


   //$operacion, deberia ser HTTP,PUT,POS,etc
   static public function autenticar($user, $passwd, $url, $operacion) {
           $autorizacion=new ApiScanntech();
           $autorizacion -> convertir_base64($user, $passwd);

            if ($autorizacion != "Error") {
                
                // Cabecera
                $cabecera = array(
                        'Accept: application/json',
                        'Content-Type: application/json',
                        'method'  => $operacion,
                        // 'Authorization: Basic '. strval($autorizacion)
                        'Authorization: Basic '.  base64_encode("$user:$passwd")
                    );

                //  Iniciar curl
                $ch = curl_init();

                // Setear la cabecera en formato Json y con autorizacion
                curl_setopt($ch, CURLOPT_HTTPHEADER, $cabecera);

                // Desabilita la verificacion SSL
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                // Si se retorna una respuesta y es falsa la imprime
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Setear la url
                curl_setopt($ch, CURLOPT_URL,$url);

                // Ejecutar curl
                $resultado=curl_exec($ch);
                
                // Cerrar curl
                curl_close($ch);

                //echo $resultado;
                             
                return $resultado;

            } else {
                return null;
            }
    
       }// Fin función autenticar


 }//Fin class ApiScanntech
