<?php
	
	//OBTENER DATOS DE LA BASE DE DATOS 
	require_once "obtener_datos.php";
	$fecha = (isset($_GET["fechaEnvio"])) ? $_GET["fechaEnvio"] : date('Y-m-d');
	$sucursal = (isset($_GET["idSucursal"])) ? $_GET["idSucursal"] : 'sucursal2';
	echo '<h3>Fecha: '. $fecha . '</h3>';
	echo '<h3>Sucursal: '. $sucursal .'</h3>';
	$objetoVentas = new ScanntechDatosVentas();
	$datosVentas = $objetoVentas->ListarVentasXFecha($fecha, $sucursal);
	echo '<h3>Json ventas:</h3>';
	echo $datosVentas;
	$datosCierre = $objetoVentas->CierreDiarioVentas($fecha, $sucursal);
	echo '<h3>Cierre:</h3>';
	echo $datosCierre;


	//CONECTAR API Y PASAR DATOS
	require_once "seguridad.php";
	$idEmpresa = 33528;
	$idLocal = 1;
	$idCaja = 1;

	$URL_BASE = "http://test.parceiro.scanntech.com/api-minoristas/api/v2/minoristas/$idEmpresa/locales/$idLocal/cajas/$idCaja/cierresPromociones?fechaConsulta=2020-10-13";
	// $URL_BASE = "http://test.parceiro.scanntech.com/api-minoristas/api/v2/minoristas/33528/locales/1/solicitudes/movimientos";
	$user = "integrador_test@layuntamayorista.com.ar";
	$passwd = "integrador";
	$operacion = "GET";

	$conectarAPI = new ApiScanntech();
	$resp = $conectarAPI -> autenticar($user, $passwd, $URL_BASE, $operacion);

	print_r($resp);

?>