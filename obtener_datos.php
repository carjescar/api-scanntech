<?php

require_once "../../controladores/ventas.controlador.php";
require_once "../../modelos/ventas.modelo.php";

require_once "../../controladores/productos.controlador.php";
require_once "../../modelos/productos.modelo.php";

class ScanntechDatosVentas{

 	/*=============================================
 	LISTAR VENTAS EN FORMATO JSON PARA ENVIAR A SCANNTECH
  	=============================================*/ 
	public function ListarVentasXFecha($fechaListado, $sucursal){

		date_default_timezone_set('America/Argentina/Mendoza');

		$fecha = (isset($fechaListado)) ? $fechaListado : date('Y-m-d');

  		$ventas = ControladorVentas::ctrMostrarVentasScanntech($fecha, $sucursal);
 		
  		// if(count($ventas) == 0){

  		// 	echo '{[]}';

		  // 	return;
  		// }	
		
  		$datosJson = '[';

		// for($i = 0; $i < count($ventas); $i++){
  		foreach ($ventas as $key => $venta) {

			$datosJson .='{
				"fecha" : "'.$venta["fecha"].'",
				"numero" : "'.$venta["id"].'",
				"descuentoTotal" : 0.00,
				"recargoTotal" : 0.00,
				"codigoMoneda" : "032",
				"cotizacion" : 1,
				"total" : "'.$venta["total"].'",
				"cancelacion": false,
				"documentoCliente": "'.$venta["documentoCliente"].'",
				"detalles": [';
			
			$productos = json_decode($venta["productos"], true);

			foreach ($productos as $key => $producto) {
			
				$precioUnitario = $producto["precioVenta"] / $producto["cantidad"];
				$datosProducto = ControladorProductos::ctrMostrarProductosScanntech($producto["id"], $venta["id_vendedor"]);
				$datosJson .='{
					"codigoArticulo": "'.$datosProducto["id"].'",
					"codigoBarras": "'.$datosProducto["codigo"].'",
					"descripcionArticulo": "'.$producto["descripcion"].'",
					"cantidad": '.$producto["cantidad"].',
					"importeUnitario": '.$precioUnitario.',
					"importe": '.$producto["total"].',
					"descuento": 0.00,
					"recargo": 0.00,
					"clasificacionDescuento": null
				},';
			}

			$datosJson = substr($datosJson, 0, -1);

			$datosJson .='],
			"pagos": [';

			/*Valores codigoTipoPago
			9 = Efectivo
			10 = Tarjeta de Crédito
			11 = Cheque
			12 = Vale (Ticket Alimentación, etc)
			13 = Tarjeta de Débito*/

			$pagos = json_decode($venta["metodo_pago"], true);

			foreach ($pagos as $key => $pago) {

				if($pago["entrega"] > 0){

					$codigo = explode("-", $pago["tipo"]);
					switch ($codigo[0]) {
						case 'TD':
							$tipoPago = 13;
							break;
						case 'TC':
							$tipoPago = 10;
							break;
						default:
							$tipoPago = 9;
							break;
					}

					$datosJson .='{
				        "codigoTipoPago": '.$tipoPago.',
				        "codigoMoneda": "032",
				        "importe": '.$pago["entrega"].',
				        "cotizacion": 1.00,
				        "documentoCliente": null,
				        "bin": null,
				        "ultimosDigitosTarjeta": null,
				        "numeroAutorizacion": null,
				        "codigoTarjeta": null
				   },';
				}
			}

			$datosJson = substr($datosJson, 0, -1);
			
			$datosJson .=']
			},';

		}

		$datosJson = substr($datosJson, 0, -1);

		$datosJson .= ']';


		$datosJson = '[{ "fecha" : "2020-09-05 16:55:45", "numero" : "641159", "descuentoTotal" : 0.00, "recargoTotal" : 0.00, "codigoMoneda" : "032", "cotizacion" : 1, "total" : "860.06", "cancelacion": false, "documentoCliente": "36711644", "detalles": [{ "codigoArticulo": "357", "codigoBarras": "7790036972602", "descripcionArticulo": "LECHE CHOCOLATADA BAGGIO X 200 MML", "cantidad": 7, "importeUnitario": 12.4, "importe": 86.8, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "9284", "codigoBarras": "7798171200147", "descripcionArticulo": "PAN DE MAXIHAMBURGUESA GABY 310 gr Combo", "cantidad": 1, "importeUnitario": 169.98, "importe": 169.98, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "9286", "codigoBarras": "664", "descripcionArticulo": "HAMBURGUESA X 2 UNID PARA COMBO Combo", "cantidad": 1, "importeUnitario": 169.98, "importe": 169.98, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "201", "codigoBarras": "7790250056775", "descripcionArticulo": "ROLLO DE COCINA SUSSEX X3u 50PAÑOS", "cantidad": 10, "importeUnitario": 45.99, "importe": 459.9, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null }], "pagos": [{ "codigoTipoPago": 9, "codigoMoneda": "032", "importe": 860.06, "cotizacion": 1.00, "documentoCliente": null, "bin": null, "ultimosDigitosTarjeta": null, "numeroAutorizacion": null, "codigoTarjeta": null }] },{ "fecha" : "2020-09-05 16:45:27", "numero" : "641127", "descuentoTotal" : 0.00, "recargoTotal" : 0.00, "codigoMoneda" : "032", "cotizacion" : 1, "total" : "92.72", "cancelacion": false, "documentoCliente": "0", "detalles": [{ "codigoArticulo": "9053", "codigoBarras": "7798144941046", "descripcionArticulo": "PEPAS CON CHOCOLATE x 100 gr DIMAX", "cantidad": 1, "importeUnitario": 46.36, "importe": 46.36, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "9053", "codigoBarras": "7798144941046", "descripcionArticulo": "PEPAS CON CHOCOLATE x 100 gr DIMAX", "cantidad": 1, "importeUnitario": 46.36, "importe": 46.36, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null }], "pagos": [{ "codigoTipoPago": 9, "codigoMoneda": "032", "importe": 92.72, "cotizacion": 1.00, "documentoCliente": null, "bin": null, "ultimosDigitosTarjeta": null, "numeroAutorizacion": null, "codigoTarjeta": null }] },{ "fecha" : "2020-09-05 16:44:52", "numero" : "641126", "descuentoTotal" : 0.00, "recargoTotal" : 0.00, "codigoMoneda" : "032", "cotizacion" : 1, "total" : "224.87", "cancelacion": false, "documentoCliente": "0", "detalles": [{ "codigoArticulo": "8697", "codigoBarras": "7798144940230", "descripcionArticulo": "TOSTADAS DIMAX X 200 GR", "cantidad": 1, "importeUnitario": 90.76, "importe": 90.76, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "8308", "codigoBarras": "7798031470161", "descripcionArticulo": "WAKAS PASTA GARBANZO X 250 gr", "cantidad": 1, "importeUnitario": 134.11, "importe": 134.11, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null }], "pagos": [{ "codigoTipoPago": 9, "codigoMoneda": "032", "importe": 224.87, "cotizacion": 1.00, "documentoCliente": null, "bin": null, "ultimosDigitosTarjeta": null, "numeroAutorizacion": null, "codigoTarjeta": null }] },{ "fecha" : "2020-09-05 16:36:42", "numero" : "641105", "descuentoTotal" : 0.00, "recargoTotal" : 0.00, "codigoMoneda" : "032", "cotizacion" : 1, "total" : "200.44", "cancelacion": false, "documentoCliente": "0", "detalles": [{ "codigoArticulo": "1584", "codigoBarras": "7790150434345", "descripcionArticulo": "CALDO SABORIZANTE FINAS HIERBAS X7.5G", "cantidad": 1, "importeUnitario": 15, "importe": 15, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "124", "codigoBarras": "7793065001374", "descripcionArticulo": "MAYONESA COSTA DEL SOL X238g", "cantidad": 1, "importeUnitario": 25.48, "importe": 25.48, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "201", "codigoBarras": "7790250056775", "descripcionArticulo": "ROLLO DE COCINA SUSSEX X3u 50PAÑOS", "cantidad": 1, "importeUnitario": 45.99, "importe": 45.99, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "201", "codigoBarras": "7790250056775", "descripcionArticulo": "ROLLO DE COCINA SUSSEX X3u 50PAÑOS", "cantidad": 1, "importeUnitario": 45.99, "importe": 45.99, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "511", "codigoBarras": "7791813555100", "descripcionArticulo": "7UP RETORNABLE X 2.25", "cantidad": 1, "importeUnitario": 67.98, "importe": 67.98, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null }], "pagos": [{ "codigoTipoPago": 9, "codigoMoneda": "032", "importe": 200.44, "cotizacion": 1.00, "documentoCliente": null, "bin": null, "ultimosDigitosTarjeta": null, "numeroAutorizacion": null, "codigoTarjeta": null }] },{ "fecha" : "2020-09-05 11:49:44", "numero" : "640349", "descuentoTotal" : 0.00, "recargoTotal" : 0.00, "codigoMoneda" : "032", "cotizacion" : 1, "total" : "89.14", "cancelacion": false, "documentoCliente": "0", "detalles": [{ "codigoArticulo": "2158", "codigoBarras": "7790150441459", "descripcionArticulo": "SOPA TIPO CRIOLLA ALICANTE X72G", "cantidad": 1, "importeUnitario": 44.57, "importe": 44.57, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null },{ "codigoArticulo": "2158", "codigoBarras": "7790150441459", "descripcionArticulo": "SOPA TIPO CRIOLLA ALICANTE X72G", "cantidad": 1, "importeUnitario": 44.57, "importe": 44.57, "descuento": 0.00, "recargo": 0.00, "clasificacionDescuento": null }], "pagos": [{ "codigoTipoPago": 9, "codigoMoneda": "032", "importe": 89.14, "cotizacion": 1.00, "documentoCliente": null, "bin": null, "ultimosDigitosTarjeta": null, "numeroAutorizacion": null, "codigoTarjeta": null }] }]';

		return $datosJson;

	}

	//CIERRE DE OPERACIONES DIARIAS SCANNTECH
	public function CierreDiarioVentas($fechaCierre, $sucursal){
	
		$ventas = ControladorVentas::ctrDatosCierreDiarioVentas($fechaCierre, $sucursal);

		$datosJson = '{
   		 "fechaVentas": "'.$fechaCierre.'",
		 "montoVentaLiquida": '.round($ventas["ventas"],2).',
   		 "montoCancelaciones": 0,
   		 "cantidadMovimientos": '.$ventas["movimientos"].',
   		 "cantidadCancelaciones": 0
		}';

		$datosJson = '{ "fechaVentas": "2020-09-05", "montoVentaLiquida": 145437.38, "montoCancelaciones": 0, "cantidadMovimientos": 163, "cantidadCancelaciones": 0 }';
		return $datosJson;
	}

}